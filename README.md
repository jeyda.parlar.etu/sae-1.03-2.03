# Virtual box
>La premiere semaine de la SAE, nous avions preparé deux machines virtuels avec deux méthodes d'installation de l'OS Debian.

![Vu des deux machines](/capture2machines.png)

**1.Preparation de la premiere VM:**

Pour la préparation de la machine virtuelle, nous avons suivis avec attention les instructions indiqués sur le sujet.

Pour l'installation de l'OS Debian, au lieu d'effectuer une installaion du fichier du site officiel de Debian nous avions chercher cela dans les fichiers existants sur l'ordinateur de l'IUT. 

Pour la suite de l'installation, nous avons suivis les instruction du sujet mais on s'est également servis du sujet de la SAE d'installation de poste du premier semestre. 

Suite au démarrage de la machine virtuelle, nous avions fait les etapes de configuration comme :

* L'ajout du profil user dans le groude sudo :

![photo de la commande](/adduser.png)

* L'installation des suppléments invité, qui nous permet de redimentioner l'écran de la machine écran :
![photo du rendu](/appercumachine1.png)

Ensuite, nous avons vérifier nos systèmes pour vérifier que tout est bien, est si cela n'était pas le cas, nous avions pris le temps de réctifier toutes erreures. 

![Aperçu des aspects de la premiere machine](/capture1eremachine.png) 


Suite à cela, nous avions regarder la documentation de Debian pour avoir un appercu général de ce qui nous est proposé.



**2. Préparation de la deuxième VM préconfiguré**

Pour cette deuxième machine, nous avions effectuer une installation préconfiguré, d'une maniére automatisé. 

Pour cela, nous avons d'abord préparer la nouvelle machine de la même maniere que la précédente. Ensuite, nous avons téléchargé le fichier .zip depuis moodle afin de pouvoir s'en servir  de ***SAE203-Debian.viso***, que l'on va inséré dans le lecteur optique de la machine virtuelle. Mais avant de la lancer, on a ouvert le terminal de notre poste pour inserer les lignes de commandes suivantes : 
> `
sed -i -E "s/(--iprt-iso-maker-file-marker-bourne-sh).*$/\1=$(cat /proc/sys/kernel/random/uuid)/" S203-Debian12.viso
`
Suite à l'execution de cette commande, on insére le fichier ***SAE203-Debian.viso*** dans le lecteur optique puis on lance la machine.

![photo machine preconfig](/autozvconfig.png)

Nous constatons qu'il n'y a pas d'inteface graphique, mais que tout a éte configurer et on a pu se connecter en tant que *user* et *root*.

Nous avons également effectuer des configurations comme l'ajout de user dans le groupe sudo.
![photo de la commande](/sudo.png)

![apercu des apsects de la deuxieme machine](/capture2ememachine.png)

<meta charset=”utf-8>
<link rel="stylesheet" href="style.css">

## **Git**  

### Questions et instalations :  

* ### Qu’est-ce que le logiciel gitk ? Comment se lance-t-il ?  

Gitk est un navigateur de dépôt graphique, il permet d'explorer et de visualiser l'historique d'un dépôt git. Il se lance en utilisant la commande `gitk&`.  

![photo de gitK](img/gitk.png)  

[Liens de la source](https://www.atlassian.com/fr/git/tutorials/gitk)
  
* ### Qu’est-ce que le logiciel git-gui ? Comment se lance-t-il ?  

C'est une interface graphique de Git basée sur Tcl/Tk. Il se lance en utilisant la commande `git gui`.  

![photo de git-gui](img/git-gui.png)  

[Liens de la source](https://git-scm.com/docs/git-gui/fr)  

* ###  Pourquoi avez-vous choisi ce logiciel ?  

Nous avons choisi GitKraken car il offre une interface visuelle intuitive et plus riche en fonctionnalités, de plus les commits sont coloré ainssi que la représentations d'arborescence interactive, ce qui facilite la gestion de branches et la détection des conflits.  

![photo de git-gui](img/git-kraken.png)  

[Liens de la source](https://www.gitkraken.com/)  

* ### Comment l’avez vous installé ?  

Nous l'avons installer grâce aux commandes :  

`wget https://release.gitkraken.com/linux/gitkraken-amd64.deb`    

`sudo apt install ./gitkraken-amd64.deb`  

[Liens de la source](https://help.gitkraken.com/gitkraken-client/how-to-install/) 

* ### Comparer-le aux outils inclus avec git (et installé précédemment) ainsi qu’avec ce qui serait fait

Gitub est un service d'hébergement de dépôts Git en ligne tandis que Git-Kraken, est une application cliente de Git offrant une interface graphique pour simplifier les opérations Git.    

Dpour accéder ae plus Gitub nécessite une connexion web ux dépôts tandit que Git-Kraken fonctionne de manière autonome, offrant une visualisation claire des workflows Git.   

Pour finir Git-Kraken est apprécié pour sa facilité d'utilisation et ses outils visuels avancés pour la gestion des dépôts, même les GAFAM se servent de ce logicielle cela demontre dons éfficasité.  


## **Gitea**

## Questions :

* ### Qu’est-ce que Gitea ?  
  
Gitea est une forge logicielle web, il fournit une interface web pour gérer vos différents dépôts git et d'organiser vos projets avec plusieurs personnes.

[Liens de la source](https://nyleza.com/blog/decouverte-et-installation-gitea-gestionnaire-repo-git/) 

* ### À quels logiciels bien connus dans ce domaine peut-on le comparer (en citer au moins 2) ?  

On peut le comparer a Gitub ou encore a Gitlab

[Liens de la source](https://nyleza.com/blog/decouverte-et-installation-gitea-gestionnaire-repo-git/)

## Instalations :

Tout d'abord, nous avions commencé par la redirection de port. Cela est pour préparer l'installation de **_Gitea_**.  
Pour cela, dans la partie configuration disponible pour modifer les paramètres, nous avions ajouter une redirection se nommant gitea utilisant le protocole TCP avec un port hôte 3000 et un port invité 3000. 
 ![photo montrant redirection de port](img/port.png)

Maintenant, nous pouvons commencer **l'installation de Gitea**.

Comme indiqué dans l'énoncé, nous avions suivis les étapes nécessaire de m'installation de Gitea de binaire.  
Pour cela, nous avions choisis le fichier _gitea-1.21.9-linux-amd64_. L'installation à été faites avec `wget` , grâce à la commande `wget -O gitea1 https://dl.gitea.com/gitea/1.21.9/gitea-1.21.9-linux-amd64`.  
![Résultat de l'execution de la commande](img/suivre-étapes.png)  


Suite à cela, nous avons également téléchargé un deuxième fichier pour le binaire précedent, finissant par `.asc`. Pour cela, on a constaté qu'il faut faire attention à comment se nomme le fichier dans la commande `wget`.  
Après l'obtention de ces deux fichiers, pour vérifier le binaire, nous avons executé les commandes suivantes : 
```
gpg --keyserver keys.openpgp.org --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
gpg --verify gitea2 gitea1

```
Après avoir vérifié que tout est bon :thumbsup: , on passe à la préparation de l'environment. Pour cela, en tant que root, on:
- crée un utilisateur avec cette commande: 
```
adduser \
 --system \
 --shell /bin/bash \
 --gecos 'Git Version Control' \
 --group \
 --disabled-password \
 --home /home/git \
 git

 ```
- crée le structure de chemins nécessaire:
```
mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea
```
- copié le biniare Gitea dans le chemin demandé:
  ```
  cp gitea /usr/local/bin/gitea
  ```

Et le reste des étapes restantes...  


























